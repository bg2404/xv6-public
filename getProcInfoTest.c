#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"
#include "processInfo.h"

int main(int argc, char* argv[]) {
    int pid;
    if(argc < 2) {
        printf(0, "PID should be provided as argument\n");
        exit();
    }
    pid = atoi(argv[1]);
    struct processInfo procInfo;
    int status = getProcInfo(pid, &procInfo);
    if(status < 0) {
        printf(0, "Not Found!\n");
    } else {
        printf(0, "PID = %d\n", procInfo.ppid);
        printf(0, "PSIZE = %d\n", procInfo.psize);
        printf(0, "NUM_PROC_INFO = %d\n", procInfo.numberContextSwitches);
    }
    exit();
}