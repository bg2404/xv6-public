#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"
#include "processInfo.h"

int main(int argc, char* argv[]) {
    int pid;
    if(argc < 2) {
        printf(0, "PID should be provided as argument\n");
        exit();
    }
    pid = atoi(argv[1]);
    int status = get_burst_time(pid);
    if(status < 0) {
        printf(0, "PID not found\n");
    } else {
        printf(0, "%d\n", status);
    }
    exit();
}