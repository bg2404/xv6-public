#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"
#include "processInfo.h"

int main(int argc, char* argv[]) {
    int pid;
    int burst_time;
    if(argc < 3) {
        printf(0, "PID and Burst time should be provided\n");
        exit();
    }
    pid = atoi(argv[1]);
    burst_time = atoi(argv[2]);
    int status = set_burst_time(pid, burst_time);
    if(status < 0) {
        printf(0, "PID not found\n");
    } else {
        printf(0, "Burst time set successfully\n");
    }
    exit();
}