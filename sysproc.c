#include "types.h"
#include "x86.h"
#include "defs.h"
#include "date.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"

int
sys_fork(void)
{
  return fork();
}

int
sys_exit(void)
{
  exit();
  return 0;  // not reached
}

int
sys_wait(void)
{
  return wait();
}

int
sys_kill(void)
{
  int pid;

  if(argint(0, &pid) < 0)
    return -1;
  return kill(pid);
}

int
sys_getpid(void)
{
  return myproc()->pid;
}

int
sys_sbrk(void)
{
  int addr;
  int n;

  if(argint(0, &n) < 0)
    return -1;
  addr = myproc()->sz;
  if(growproc(n) < 0)
    return -1;
  return addr;
}

int
sys_sleep(void)
{
  int n;
  uint ticks0;

  if(argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
    if(myproc()->killed){
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
  }
  release(&tickslock);
  return 0;
}

// return how many clock tick interrupts have occurred
// since start.
int
sys_uptime(void)
{
  uint xticks;

  acquire(&tickslock);
  xticks = ticks;
  release(&tickslock);
  return xticks;
}

int
sys_wolfie(void)
{
    void *buf;
    uint size;
    argptr(0, (void *)&buf, sizeof(buf));
    argptr(1, (void *)&size, sizeof(size));
    return wolfie(buf, size);
}

int
sys_getNumProc()
{
  return getNumProc();
}

int
sys_getMaxPid()
{
  return getMaxPid();
}

int
sys_getProcInfo()
{
  int pid;
  struct processInfo *procInfo;
  argptr(0, (void *)&pid, sizeof(pid));
  argptr(1, (void *)&procInfo, sizeof(procInfo));
  return getProcInfo(pid, procInfo);
}

int
sys_set_burst_time(void)
{
  int pid;
  int n;
  argptr(0, (void *)&pid, sizeof(pid));
  argptr(1, (void *)&n, sizeof(n));
  return set_burst_time(pid, n);
}

int
sys_get_burst_time(void)
{

  int pid;
  uint n;
  argptr(0, (void *)&pid, sizeof(pid));
  argptr(1, (void *)&n, sizeof(n));
  return get_burst_time(pid);
}