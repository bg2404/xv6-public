#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"
#include "processInfo.h"

int main(int argc, char* argv[]) {
    int size;
    if(argc < 2) {
        size = 0;
    } else {
        size = atoi(argv[1]);
    }
    // char* buf = (char*)malloc(sizeof(char)*size);
    // int r = wolfie(buf, size);
    // if(r > 0) {
    //     printf(0, "%s\n", buf);
    // } else {
    //     printf(0, "Buffer too small!\n");
    // }
    // //getProcInfo
    // struct processInfo procInfo;
    // int status = getProcInfo(size, &procInfo);
    // if(status == -1) {
    //     printf(0, "Not Found!\n");
    // } else {
    //     printf(0, "PID = %d\n", procInfo.ppid);
    //     printf(0, "PSIZE = %d\n", procInfo.psize);
    //     printf(0, "NUM_PROC_INFO = %d\n", procInfo.numberContextSwitches);
    // }
    // burst time
    set_burst_time(size, 10);
    printf(0, "%d\n", get_burst_time(size));
    exit();
}